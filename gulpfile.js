var gulp = require('gulp');
var tslint = require('gulp-tslint');
var connect = require('gulp-connect');
var browserify = require('browserify');
var source = require('vinyl-source-stream');
var watchify = require('watchify');
var tsify = require('tsify');
var sass = require('gulp-sass');
var browserSync = require('browser-sync').create();
var SYNC_PORT   = 8081;

var viewPath = {
    src: ['src/views/*.html']
};
var stylePath = {
    src: ['src/styles/css/*.css']
};
var indexPath = {
    src: ['src/index.html']
};

// copying files
gulp.task('copyViews', function () {
    return gulp.src(viewPath.src)
        .pipe(gulp.dest('dist/views'));
});

// copying css
gulp.task('copyStyles', function () {
    return gulp.src(stylePath.src)
        .pipe(gulp.dest('dist/css'));
});

// copying main index.html into root dir
gulp.task('copyIndex', function () {
    return gulp.src(indexPath.src)
        .pipe(gulp.dest('.'));
});

// create copy group static files
gulp.task('copyStaticFiles', [
    'copyViews',
    'copyStyles',
    'copyIndex'
]);

// compile sass into css and move to css directory
gulp.task('sassCompile', function () {
  return gulp.src('./src/styles/sass/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./dist/css'));
});

// Static files not compile, to copy if change
gulp.task('static:watch', function() {
    gulp.watch(['./src/views/*.html'], ['copyViews', browserSync.reload]);
    gulp.watch(['./src/styles/css/*.css'], ['copyStyles', browserSync.reload]);
    gulp.watch(['./src/index.html'], ['copyIndex', browserSync.reload]);
});

gulp.task('sass:watch', function () {
    gulp.watch('./src/styles/sass/*.scss', ['sassCompile', browserSync.reload]);
});

gulp.task('ts:watch', ['ts:compile'], function() {
    gulp.watch([
      './src/*.ts',
      './src/*/*.ts'
    ], ['ts:compile', 'tslint', browserSync.reload]);
});

gulp.task('tslint', function() {
  return gulp.src('./src/**/*.ts')
    .pipe(tslint({
        // contains rules in the tslint.json format
        configuration: 'ts_lint_cfg.json'
    }))
    .pipe(tslint.report());
});

// Static Server watching files
gulp.task('browser:sync', function() {
    browserSync.init({
        port: SYNC_PORT,
        domain: process.env.IP
    });
});

// Server start function task (NodeJS)
gulp.task('connectServer', function () {
  	connect.server({
      root: '.',
      port: process.env.PORT,
      host: process.env.IP
  	});
});

var watchedBrowserify = watchify(browserify({
    basedir: 'src',
    debug: true,
    entries: ['main.ts'],
    cache: {},
    packageCache: {}
}).plugin(tsify));

gulp.task('ts:compile', function () {
    return watchedBrowserify
      .bundle()
      .on('error', function (error) { 
        console.log("\n");
        console.error('\x1b[31m ', error.toString()); 
      })
      .pipe(source('bundle.js'))
      .pipe(gulp.dest('dist'));
});

gulp.task('default', [
    'connectServer',
    'copyStaticFiles',
    'sassCompile',
    'ts:compile',
    'tslint',
    'ts:watch',
    'static:watch',
    'sass:watch',
    'browser:sync'
]);
