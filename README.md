### This is simple setup of TypeScript with gulp, browserify, watchify helpers. ###
This guide was build by using example of quick start manual on quick setup [link](https://www.typescriptlang.org/docs/handbook/gulp.html )

### The expected project is to be laid out as such: ###

![tree.jpg](https://bitbucket.org/repo/Agg8ppK/images/2311038289-tree.jpg)

## Quick setup: ##

1. Clone project on source : 
    * [project link](https://romanhrb@bitbucket.org/romanhrb/ts_setup.git)
2. Run command ( npm package manager required ): 
    * npm install
3. To start process of gulp task manager, run by command:
    * gulp

